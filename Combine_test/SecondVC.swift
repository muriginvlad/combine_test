//
//  SecondVC.swift
//  Combine_test
//
//  Created by Владислав on 20.01.2021.
//

import UIKit
import Combine
import Alamofire


class SecondVC: UIViewController {
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var labelTest: UILabel!
    @IBOutlet weak var downloadProgressLabel: UIProgressView!
    
    var buffer: NSMutableData = NSMutableData()
    var expectedContentLength = 0
    
    private var observation: NSKeyValueObservation?
    
   
    let subject = PassthroughSubject<Double, Never>()
    var subscriber: AnyCancellable?

    var tokens: Set<AnyCancellable> = []
    var first: AnyPublisher<Double, AFError>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadProgressLabel.progress = 0.0
    
//        subscriber = subject.sink(receiveValue: {
//            self.downloadProgressLabel.progress = Float($0)
//            print($0)
//            })
        _ = first?.sink(receiveCompletion: { error in
            print(error)
        }, receiveValue: { prog in
            print(prog)
        })
        
    }
    
    
    
    @IBAction func postTapped(_ sender: Any) {
        let testData = ["username":"Vlad","message":"Hi!"]
        postTest(parameters: testData)
    }
    
    @IBAction func getTapped(_ sender: Any) {
        getAlamofireTest()
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        deleteMethod()
    }
    
    @IBAction func firstButton(_ sender: Any) {
        labelTest.text = ""
        downloadProgressLabel.progress = 0.0
    }
    
    
    @IBAction func startDownloadTapped(_ sender: Any) {
        //        let url = URL(string: "https://speedtest.selectel.ru/10MB")!
        //        fetchFile(url: url)
        getDownloadAlamofireCombineTest()
    }
    
    //MARK: -Alamofire
    
    func getAlamofireTest() {
        
        let url = "https://jsonplaceholder.typicode.com/comments?postId=1"
         AF.request(url).responseJSON { response in
            if let objects = try? response.result.get(), let jsonData = objects as? NSArray {
                print(jsonData[0])
            }
        }
        
    }
    
    
    func postAlamofireTest() {
        let url = "https://jsonplaceholder.typicode.com/posts"
        
    }
    
    func getDownloadAlamofireTest(){
        
        let url = "https://speedtest.selectel.ru/10MB"
        AF.download(url).downloadProgress(queue:.main) { progress in
        self.subject.send(progress.fractionCompleted)
        }.response { resp in
            print("")
        }
    }
    
    
    
    func getDownloadAlamofireCombineTest(){
        
        let url = "https://speedtest.selectel.ru/10MB"
        
        first = AF.download(url).downloadProgress { progress in
            //    print(progress.fractionCompleted)
        }.response { resp in
            print("")
        }.publishDecodable(type: Double.self)
        .value()
        
        
        //        .sink { (Subscribers.Completion<AFError>) in
        //            <#code#>
        //        } receiveValue: { (<#Double#>) in
        //            <#code#>
        //        }
        //
        //        }.store(in: &tokens)
        
        
        //            AF.download(url).downloadProgress(queue:.main) { progress in
        //        self.subject.send(progress.fractionCompleted)
        //        }.response { resp in
        //            print("")
        //        }
    }
    
    
    //MARK: -URLSession
    
    
    func getTest(){
        
        let url = URL(string: "https://jsonplaceholder.typicode.com/comments?postId=1")!
        let session = URLSession.shared
        let task = session.dataTask(with: url) {(data, response, error) in
            if let response = response as? HTTPURLResponse {
                print(response.statusCode)
            }
            
            guard let data = data else { return }
            do{let json = try JSONSerialization.jsonObject(with: data, options: [])
                guard let jsonDict = json as? NSArray else { return }
                
                let test = jsonDict[0]
                print(test)
            } catch { print(error) }
        }
        
        observation = task.progress.observe(\.fractionCompleted) { progress, _ in
            DispatchQueue.main.async {
                self.downloadProgressLabel.progress = Float(progress.fractionCompleted)
            }
            
            print("progress: ", progress.fractionCompleted)
        }
        
        task.resume()
        
    }
    
    
    
    func postTest(parameters: Any){
        
        let url = URL(string: "https://jsonplaceholder.typicode.com/posts")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        session.dataTask(with: request) { data, response, error in
            if let response = response as? HTTPURLResponse {
                print(response.statusCode)
            }
            
            guard let data = data else { return }
            do{let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
            } catch { print(error) }
        }.resume()
    }
    
    func deleteMethod() {
        
        let url = URL(string: "https://jsonplaceholder.typicode.com/posts/1")!
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        
        let session = URLSession.shared
        session.dataTask(with: request) { data, response, error in
            if let response = response as? HTTPURLResponse {
                print(response.statusCode)
            }
            
            guard let data = data else { return }
            do { _ = try? JSONSerialization.jsonObject(with: data, options: [])
                print("Data deleted!")
            } catch { print(error) }
        }.resume()
    }
    
    
    
    func getDownloadTest(){
        
        downloadProgressLabel.progress = 0.0
        labelTest.text = "Загрузка"
        
        let url = URL(string: "https://speedtest.selectel.ru/10MB")!
        var session = URLSession.shared
        let task = session.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            print(data)
        }
        
        observation = task.progress.observe(\.fractionCompleted) { progress, _ in
            
            DispatchQueue.main.async {
                self.downloadProgressLabel.progress = Float(progress.fractionCompleted)
                if progress.fractionCompleted >= 1 {
                    self.labelTest.text = "Загрузка завершина"
                }
                
            }
        }
        task.resume()
        
        
        
    }
}

extension SecondVC: URLSessionDelegate, URLSessionDataDelegate {
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        labelTest.text = "Идет загрузка"
        buffer.append(data)
        let percentageDownloaded = Float(buffer.length) / Float(expectedContentLength)
        downloadProgressLabel.progress =  percentageDownloaded
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: (URLSession.ResponseDisposition) -> Void) {
        expectedContentLength = Int(response.expectedContentLength)
        completionHandler(URLSession.ResponseDisposition.allow)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        labelTest.text = "Загрузка завершина"
        downloadProgressLabel.progress = 1.0
    }
    
    func fetchFile(url: URL) {
        downloadProgressLabel.progress = 0.0
        labelTest.text = "Начaло загрузки"
        let configuration = URLSessionConfiguration.default
        let mainQueue = OperationQueue.main
        var session = URLSession(configuration: configuration, delegate: self, delegateQueue: mainQueue)
        var dataTask = session.dataTask(with: URLRequest(url: url))
        
        dataTask.resume()
    }
    
}
